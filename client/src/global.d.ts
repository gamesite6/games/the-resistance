/// <reference types="svelte" />
/// <reference types="vite/client" />

type PlayerId = number;

type GameState = {
  players: PlayerState[];
  leader: PlayerId;
  phase: Phase;
  missions: Mission[];
  failedVotes: number;
  speech: {
    [playerId: string]: Speech;
  };
};

type Mission = {
  team: PlayerId[];
  cards: MissionCard[];
  winner: Affiliation;
};

type Phases = {
  RoleAssigning?: undefined;
  TeamProposing?: undefined;
  TeamVoting?: undefined;
  TeamVoteReveal?: undefined;
  MissionInProgress?: undefined;
  MissionReveal?: undefined;
  Assassination?: undefined;
  GameComplete?: undefined;
};

type RoleAssigning = Omit<Phases, "RoleAssigning"> & {
  RoleAssigning: {
    ready: PlayerId[];
  };
};
type TeamProposing = Omit<Phases, "TeamProposing"> & {
  TeamProposing: {};
};
type TeamVoting = Omit<Phases, "TeamVoting"> & {
  TeamVoting: {
    team: PlayerId[];
    votes: { [playerId: string]: boolean };
  };
};
type TeamVoteReveal = Omit<Phases, "TeamVoteReveal"> & {
  TeamVoteReveal: {
    team: PlayerId[];
    votes: { [playerId: string]: boolean };
    ready: PlayerId[];
  };
};
type MissionInProgress = Omit<Phases, "MissionInProgress"> & {
  MissionInProgress: {
    cards: { [playerId: string]: MissionCard | null };
  };
};
type MissionReveal = Omit<Phases, "MissionReveal"> & {
  MissionReveal: {
    team: PlayerId[];
    cards: MissionCard[];
    ready: PlayerId[];
  };
};
type Assassination = Omit<Phases, "Assassination"> & {
  Assassination: {};
};
type GameComplete = Omit<Phases, "GameComplete"> & {
  GameComplete: {
    winners: Affiliation;
  };
};

type Affiliation = "Resistance" | "Spies";

type Phase =
  | RoleAssigning
  | TeamProposing
  | TeamVoting
  | TeamVoteReveal
  | MissionInProgress
  | MissionReveal
  | Assassination
  | GameComplete;

type PlayerState = { id: PlayerId; hand: CharacterCard };

type Settings = {
  gameMode: { Avalon: {} };
  selectedPlayerCounts: PlayerId[];
};

type MissionCard = "Success" | "Fail";

type Actions = {
  Ready?: undefined;
  ProposeTeam?: undefined;
  VoteTeam?: undefined;
  PlayMissionCard?: undefined;
  Assassinate?: undefined;
};

type Ready = Omit<Actions, "Ready"> & { Ready: {} };
type ProposeTeam = Omit<Actions, "ProposeTeam"> & {
  ProposeTeam: { team: PlayerId[] };
};
type VoteTeam = Omit<Actions, "VoteTeam"> & {
  VoteTeam: { vote: boolean };
};
type PlayMissionCard = Omit<Actions, "PlayMissionCard"> & {
  PlayMissionCard: { card: MissionCard };
};
type Assassinate = Omit<Actions, "Assassinate"> & {
  Assassinate: { target: PlayerId };
};

type Action = Ready | ProposeTeam | VoteTeam | PlayMissionCard | Assassinate;

type Roles = {
  Operative?: undefined;
  Spy?: undefined;
  Merlin?: undefined;
  Assassin?: undefined;
};

type Operative = Omit<Roles, "Operative"> & { Operative: {} };
type Spy = Omit<Roles, "Spy"> & { Spy: {} };
type Merlin = Omit<Roles, "Merlin"> & { Merlin: {} };
type Assassin = Omit<Roles, "Assassin"> & { Assassin: {} };

type CharacterCard = Operative | Spy | Merlin | Assassin;

type Speeches = {
  Assassinated?: undefined;
  WasAssassinated?: undefined;
};

type Assassinated = Omit<Speeches, "Assassinated"> & {
  Assassinated: { target: PlayerId };
};
type WasAssassinated = Omit<Speeches, "WasAssassinated"> & {
  WasAssassinated: {};
};
type Speech = Assassinated | WasAssassinated;
