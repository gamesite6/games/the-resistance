import GameComponent from "./Game.svelte";
import ReferenceComponent from "./Reference.svelte";

export class Game extends HTMLElement {
  #userId?: PlayerId;
  #state?: GameState;
  #settings?: Settings;
  #app?: GameComponent;

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      this.rerender();
    }
  }
  set settings(settings: Settings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender();
    }
  }
  set userId(userId: PlayerId | undefined) {
    if (this.#userId !== userId) {
      this.#userId = userId;
      this.rerender();
    }
  }

  connectedCallback() {
    this.#app = new GameComponent({
      target: this,
      props: {
        userId: this.#userId ?? null,
        settings: this.#settings,
        state: this.#state,
      },
    });

    this.#app.$on("action", (evt) => {
      this.dispatchEvent(
        new CustomEvent("action", {
          detail: evt.detail,
          bubbles: false,
        })
      );
    });
  }

  static get observedAttributes() {
    return ["user-id"];
  }

  private rerender() {
    if (this.#app) {
      this.#app.$set({
        userId: this.#userId ?? null,
        settings: this.#settings,
        state: this.#state,
      });
    }
  }

  attributeChangedCallback(name: string, oldValue: any, newValue: any) {
    if (name === "user-id") {
      const parsed = parseInt(newValue);
      this.userId = Number.isInteger(parsed) ? parsed : undefined;
    }
  }
}

export class Reference extends HTMLElement {
  #app?: ReferenceComponent;
  #state?: GameState;
  #settings?: Settings;

  set state(state: GameState) {
    if (this.#state !== state) {
      this.#state = state;
      this.rerender();
    }
  }
  set settings(settings: Settings) {
    if (this.#settings !== settings) {
      this.#settings = settings;
      this.rerender();
    }
  }

  connectedCallback() {
    this.#app = new ReferenceComponent({
      target: this,
      props: {
        state: this.#state,
        settings: this.#settings,
      },
    });
  }

  private rerender() {
    if (this.#app) {
      this.#app.$set({
        state: this.#state,
        settings: this.#settings,
      });
    }
  }
}
