export function range(from: number, to: number): number[] {
  const size = Math.max(to - from, 0);
  const result = new Array(size);

  for (let i = 0; i < size; i++) {
    result[i] = from + i;
  }

  return result;
}

export function rangeInclusive(from: number, to: number): number[] {
  return range(from, to + 1);
}

export function reversed<T>(ts: T[]): T[] {
  let result = [...ts];
  result.reverse();
  return result;
}

export function indexed<T>(ts: T[]): [T, number][] {
  return ts.map((t, i) => [t, i]);
}

export function reordered(
  players: PlayerState[],
  userId: PlayerId | null
): PlayerState[] {
  let userIndex = players.findIndex((p) => p.id === userId);
  if (userIndex === -1) {
    return players;
  } else {
    return [...players.slice(userIndex), ...players.slice(0, userIndex)];
  }
}

export function never(_: never): any {}
