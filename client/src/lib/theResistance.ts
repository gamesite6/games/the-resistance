import { never } from "./utils";

export function isAssassin(c: CharacterCard): c is Assassin {
  return c.Assassin !== undefined;
}

export function isMerlin(c: CharacterCard): c is Merlin {
  return c.Merlin !== undefined;
}
export function isSpy(c: CharacterCard): c is Spy {
  return c.Spy !== undefined;
}
export function isOperative(c: CharacterCard): c is Operative {
  return c.Operative !== undefined;
}

export function getAffiliation(c: CharacterCard): Affiliation {
  if (isAssassin(c) || isSpy(c)) return "Spies";
  else return "Resistance";
}

export function characterEmoji(c: CharacterCard): string {
  if (isAssassin(c)) return "🧛‍♀️";
  else if (isMerlin(c)) return "🧙‍♂️";
  else if (isOperative(c)) return "🧑‍🔧";
  else if (isSpy(c)) return "🕵️";

  return never(c);
}
export function characterName(c: CharacterCard): string {
  if (isAssassin(c)) return "Assassin";
  else if (isMerlin(c)) return "Merlin";
  else if (isOperative(c)) return "Operative";
  else if (isSpy(c)) return "Spy";

  return never(c);
}

export type MissionSpec = { teamSize: number; twoFailsRequired?: true };

export function getSpyCount(playerCount: number): number {
  switch (playerCount) {
    case 5:
      return 2;
    case 6:
      return 2;
    case 7:
      return 3;
    case 8:
      return 3;
    case 9:
      return 3;
    case 10:
      return 4;
    default:
      return 0;
  }
}

export function getMissionStructure(playerCount: number): MissionSpec[] {
  switch (playerCount) {
    case 5:
      return [
        { teamSize: 2 },
        { teamSize: 3 },
        { teamSize: 2 },
        { teamSize: 3 },
        { teamSize: 3 },
      ];
    case 6:
      return [
        { teamSize: 2 },
        { teamSize: 3 },
        { teamSize: 4 },
        { teamSize: 3 },
        { teamSize: 4 },
      ];
    case 7:
      return [
        { teamSize: 2 },
        { teamSize: 3 },
        { teamSize: 3 },
        { teamSize: 4, twoFailsRequired: true },
        { teamSize: 4 },
      ];

    case 8:
      return [
        { teamSize: 3 },
        { teamSize: 4 },
        { teamSize: 4 },
        { teamSize: 5, twoFailsRequired: true },
        { teamSize: 5 },
      ];

    case 9:
      return [
        { teamSize: 3 },
        { teamSize: 4 },
        { teamSize: 4 },
        { teamSize: 5, twoFailsRequired: true },
        { teamSize: 5 },
      ];
    case 10:
      return [
        { teamSize: 3 },
        { teamSize: 4 },
        { teamSize: 4 },
        { teamSize: 5, twoFailsRequired: true },
        { teamSize: 5 },
      ];

    default:
      return [];
  }
}

export function isPlayerActive(state: GameState, playerId: PlayerId): boolean {
  const phase = state.phase;
  if (isRoleAssigning(phase)) {
    return !phase.RoleAssigning.ready.includes(playerId);
  } else if (isTeamProposing(phase)) {
    return playerId === state.leader;
  } else if (isTeamVoting(phase)) {
    return phase.TeamVoting.votes[playerId] === undefined;
  } else if (isTeamVoteReveal(phase)) {
    return !phase.TeamVoteReveal.ready.includes(playerId);
  } else if (isMissionInProgress(phase)) {
    return phase.MissionInProgress.cards[playerId] === null;
  } else if (isMissionReveal(phase)) {
    return !phase.MissionReveal.ready.includes(playerId);
  } else if (isAssassination(phase)) {
    return !!state.players.find((p) => p.id === playerId)?.hand.Assassin;
  } else if (isGameComplete(phase)) {
    return false;
  }

  return never(phase);
}

export function isRoleAssigning(phase: Phase): phase is RoleAssigning {
  return !!phase.RoleAssigning;
}
export function isTeamProposing(phase: Phase): phase is TeamProposing {
  return !!phase.TeamProposing;
}
export function isTeamVoting(phase: Phase): phase is TeamVoting {
  return !!phase.TeamVoting;
}
export function isTeamVoteReveal(phase: Phase): phase is TeamVoteReveal {
  return !!phase.TeamVoteReveal;
}
export function isMissionInProgress(phase: Phase): phase is MissionInProgress {
  return !!phase.MissionInProgress;
}
export function isMissionReveal(phase: Phase): phase is MissionReveal {
  return !!phase.MissionReveal;
}
export function isAssassination(phase: Phase): phase is Assassination {
  return !!phase.Assassination;
}
export function isGameComplete(phase: Phase): phase is GameComplete {
  return !!phase.GameComplete;
}
