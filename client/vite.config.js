import path from "path";
import { defineConfig } from "vite";
import svelte from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/the-resistance/",
  build: {
    lib: {
      entry: path.resolve(__dirname, "src", "main.ts"),
      fileName: "the-resistance",
      name: "Gamesite6_TheResistance",
    },
  },
  css: {
    modules: {},
  },
  plugins: [svelte()],
});
