package com.gamesite6.the_resistance.client

import com.gamesite6.the_resistance.shared.PlayerId
import com.raquo.laminar.builders.HtmlBuilders

object Gs6User extends HtmlBuilders {
  def apply(playerId: PlayerId) =
    htmlTag("gs6-user")(
      stringHtmlAttr("userid") := playerId.toString
    )
}
