package com.gamesite6.the_resistance.client

import com.gamesite6.the_resistance.shared.PlayerId
import com.raquo.domtypes.generic.codecs.BooleanAsTrueFalseStringCodec
import com.raquo.laminar.builders.HtmlBuilders

object Gs6PlayerBox extends HtmlBuilders {
  def apply(playerId: PlayerId) =
    htmlTag("gs6-player-box")(
      stringHtmlAttr("userid") := playerId.toString
    )

  def thinking =
    booleanReflectedAttr("thinking", BooleanAsTrueFalseStringCodec)

  def speaking =
    booleanReflectedAttr("speaking", BooleanAsTrueFalseStringCodec)

  def speechSlot = stringHtmlAttr("slot") := "speech"
}
