package com.gamesite6.the_resistance.client

import cats.implicits._
import com.gamesite6.the_resistance.shared._
import com.raquo.laminar.api.L._
import io.circe.scalajs._
import io.circe.syntax._
import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation._

@JSExportTopLevel("GameComponent")
object GameComponent {
  @JSExport
  def mount(
      container: dom.Element,
      params: js.Dictionary[js.Any]
  ): js.Dictionary[Any] = {

    val state: State = decodeJs[State](params("state")).toOption.get
    val settings: Settings = decodeJs[Settings](params("settings")).toOption.get
    val userId: Option[PlayerId] =
      decodeJs[Option[PlayerId]](params("userId")).toOption.get
    val onActionJs: js.Function1[js.Any, Unit] = params("onAction")
      .asInstanceOf[js.Function1[js.Any, Unit]]

    val stateEventBus = new EventBus[State]
    val settingsEventBus = new EventBus[Settings]
    val userIdEventBus = new EventBus[Option[PlayerId]]
    val actionEventBus = new EventBus[Action]

    implicit val stateSignal: Signal[State] =
      stateEventBus.events.toSignal(state)
    implicit val settingsSignal: Signal[Settings] =
      settingsEventBus.events.toSignal(settings)
    implicit val userSignal: Signal[Option[Player]] =
      userIdEventBus.events
        .combineWith(stateEventBus.events)
        .map {
          case (Some(userId), state) => state.players.find(_.id === userId)
          case _ => None
        }
        .toSignal(state.players.find(_.id.some === userId))
    implicit val actionWriteBus: WriteBus[Action] = actionEventBus.writer

    val actionObserver = Observer[Action] { action =>
      onActionJs(convertJsonToJs(action.asJson))
    }

    val rootNode =
      render(
        container,
        div(
          actionEventBus.events --> actionObserver,
          div(
            Seq(
              position := "absolute",
              top := "50%",
              left := "50%",
              transform := "translate(-50%, -50%)"
            ),
            CenterComponent()
          ),
          children <-- stateSignal
            .combineWith(userSignal.map(_.map(_.id)))
            .map {
              case (state, userId) =>
                sortPlayers(userId, state.players).zipWithIndex
            }
            .split { case (player, _) => player.id } {
              case (playerId, _, signal) =>
                div(
                  Seq(
                    width := "14em",
                    position := "absolute",
                    top := "50%",
                    left := "50%",
                    transform <-- (for {
                      playerCount <- stateSignal.map(_.players.size)
                      signalValue <- signal
                      (player, idx) = signalValue
                      angle = ((360.0 / playerCount) * idx).toString
                    } yield
                      s"translate(-50%, -50%) rotate(${angle}deg) translateY(23em) rotate(-${angle}deg)")
                  ),
                  child <-- signal
                    .combineWith(stateSignal)
                    .combineWith(userSignal)
                    .map {
                      case (((player, _idx), state), user) =>
                        div(
                          Seq(
                            display := "relative"
                          ),
                          gs6PlayerBox(playerId).amend(
                            if (state.leader === player.id)
                              span(span(cls := "emoji", raw"🎖️"), " Leader")
                            else
                              emptyNode,
                            gs6PlayerBox.thinking := player.isThinking(state),
                            gs6PlayerBox.speaking :=
                              state.speech.contains(player.id),
                            span(
                              gs6PlayerBox.speechSlot,
                              state.speech.get(player.id) match {
                                case Some(Speech.Assassinated(target)) =>
                                  span(gs6User(target), " is Merlin!")
                                case Some(Speech.WasAssassinated) =>
                                  if (player.hand === CharacterCard.Merlin)
                                    "You're right!"
                                  else
                                    "You're wrong!"
                                case _ => emptyNode
                              }
                            )
                          ), {
                            lazy val cardBack =
                              characterCardBack.amend(
                                fontSize := "0.5em",
                                position := "absolute",
                                bottom := "0",
                                left := "50%",
                                transform := "translate(-50%, 85%)"
                              )

                            lazy val cardFront =
                              characterCardFront(player.hand).amend(
                                fontSize := "0.5em",
                                position := "absolute",
                                bottom := "0",
                                left := "50%",
                                transform := "translate(-50%, 85%)"
                              )

                            state.phase match {
                              case Phase.RoleAssigning(ready)
                                  if !ready.contains(player.id) =>
                                emptyNode
                              case _: Phase.GameComplete => cardFront
                              case Phase.Assassination if player.isSpy =>
                                cardFront
                              case _ if user.map(_.id) === player.id.some =>
                                cardFront
                              case _ => cardBack
                            }
                          },
                        )
                    }
                )
            }
        )
      )

    js.Dictionary(
      "rootNode" -> rootNode,
      "stateEventBus" -> stateEventBus,
      "settingsEventBus" -> settingsEventBus,
      "userIdEventBus" -> userIdEventBus
    )
  }
  @JSExport
  def unmount(rootNode: RootNode): Boolean = rootNode.unmount()

  @JSExport
  def updateState(eventBus: EventBus[State], stateJs: js.Any): Unit =
    decodeJs[State](stateJs) match {
      case Right(state) => eventBus.writer.onNext(state)
      case Left(err) => throw err
    }

  @JSExport
  def updateSettings(
      eventBus: EventBus[Settings],
      settingsJs: js.Any
  ): Unit =
    decodeJs[Settings](settingsJs) match {
      case Right(settings) => eventBus.writer.onNext(settings)
      case Left(err) => throw err
    }

  @JSExport
  def updateUserId(
      eventBus: EventBus[Option[PlayerId]],
      userIdJs: js.Any
  ): Unit =
    decodeJs[Option[PlayerId]](userIdJs) match {
      case Right(userId) => eventBus.writer.onNext(userId)
      case Left(err) => throw err;
    }
}
