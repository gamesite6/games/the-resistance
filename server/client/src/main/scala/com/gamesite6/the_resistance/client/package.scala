package com.gamesite6.the_resistance

import cats.implicits._
import com.gamesite6.the_resistance.shared.Phase.{
  MissionInProgress,
  TeamVoteReveal
}
import com.gamesite6.the_resistance.shared._
import com.raquo.laminar.api.L._
import com.raquo.laminar.builders.HtmlBuilders

package object client extends HtmlBuilders {
  val gs6User: Gs6User.type = Gs6User
  val gs6PlayerBox: Gs6PlayerBox.type = Gs6PlayerBox

  lazy val cssFilter: Style[String] = style[String]("filter", "filter")
  lazy val userSelect: Style[String] =
    style[String]("userSelect", "user-select")

  def card = {
    div(
      cls := "border",
      Seq(
        boxSizing := "border-box",
        backgroundColor := "linen",
        position := "relative",
        width := "10em",
        height := "14em",
        borderRadius := "7% / 5%",
        padding := "0.75em"
      )
    )
  }

  def teamIcon(affiliation: Affiliation) = {
    affiliation match {
      case Affiliation.Resistance =>
        span(
          cls := "emoji",
          userSelect := "none",
          cssFilter := "sepia(1) hue-rotate(175deg) brightness(0.75) contrast(2)",
          raw"✊"
        )
      case Affiliation.Spies =>
        span(
          cls := "emoji",
          userSelect := "none",
          cssFilter := "sepia(1) hue-rotate(315deg) brightness(0.95) saturate(3.5)",
          raw"👁️"
        )
    }
  }

  def characterIcon(c: CharacterCard) = {
    span(
      cls := "emoji",
      userSelect := "none",
      c match {
        case CharacterCard.Merlin => raw"🧙‍♂️"
        case CharacterCard.Operative => raw"🧑‍🔧"
        case CharacterCard.Assassin => raw"🧛‍♀️"
        case CharacterCard.Spy => raw"🕵️"
      }
    )
  }

  def characterCardFront(c: CharacterCard) = {
    card.amend(
      teamIcon(c.affiliation).amend(
        fontSize := "2em",
        position := "absolute",
        top := "0.5em",
        right := "0.5em"
      ),
      characterIcon(c).amend(
        fontSize := "4.5em",
        position := "absolute",
        top := "50%",
        left := "50%",
        transform := "translate(-50%,-50%)"
      ),
      div(
        Seq(
          textAlign := "center",
          position := "absolute",
          left := "1em",
          right := "1em",
          bottom := "20%",
          transform := "translateY(50%)",
          lineHeight := "1em"
        ),
        c match {
          case CharacterCard.Operative => "Operative"
          case CharacterCard.Spy => "Spy"
          case CharacterCard.Merlin => "Merlin"
          case CharacterCard.Assassin => "Assassin"
        }
      )
    )
  }

  def characterCardBack = {
    card.amend(
      div(
        Seq(
          boxSizing := "border-box",
          position := "absolute",
          top := "0.75em",
          bottom := "0.75em",
          left := "0.75em",
          right := "0.75em",
          borderRadius := "4.1667% / 2.8333%",
          background :=
            """
              repeating-linear-gradient(
                45deg,
                #00000010,
                #00000010 5%,
                #00000020 5%,
                #00000020 10%
              ),
              repeating-linear-gradient(
                -45deg,
                #00000010,
                #00000010 5%,
                #00000020 5%,
                #00000020 10%
              ),
              radial-gradient(
                hsl(280, 17%, 48%),
                hsl(280, 13%, 37%)
              )
            """
        ),
        div(
          cls := "emoji",
          Seq(
            userSelect := "none",
            fontSize := "5em",
            height := "1.5em",
            width := "1.5em",
            textAlign := "center",
            position := "absolute",
            top := "50%",
            left := "50%",
            transform := "translate(-50%, -50%)",
            cssFilter := "sepia(0.9) hue-rotate(230deg) brightness(0.8)"
          ),
          raw"🎭"
        )
      )
    )
  }

  def missionCardBack = {
    card.amend(
      div(
        Seq(
          boxSizing := "border-box",
          position := "absolute",
          top := "0.75em",
          bottom := "0.75em",
          left := "0.75em",
          right := "0.75em",
          borderRadius := "4.1667% / 2.8333%",
          background :=
            """
              repeating-linear-gradient(
                45deg,
                #00000010,
                #00000010 5%,
                #00000020 5%,
                #00000020 10%
              ),
              repeating-linear-gradient(
                -45deg,
                #00000010,
                #00000010 5%,
                #00000020 5%,
                #00000020 10%
              ),
              radial-gradient(
                hsl(120, 17%, 48%),
                hsl(120, 13%, 37%)
              )
            """
        ),
        div(
          cls := "emoji",
          Seq(
            userSelect := "none",
            fontSize := "5em",
            height := "1.5em",
            width := "1.5em",
            textAlign := "center",
            position := "absolute",
            top := "50%",
            left := "50%",
            transform := "translate(-50%, -50%)",
            cssFilter := "sepia(0.9) hue-rotate(60deg) brightness(0.8)"
          ),
          raw"🎯"
        )
      )
    )
  }

  def missionResultIcon(c: MissionCard) =
    span(
      cls := "emoji",
      userSelect := "none",
      c match {
        case MissionCard.Fail => raw"❌"
        case MissionCard.Success => raw"✔️"
      }
    )

  def missionCardFront(c: MissionCard) = card.amend(
    missionResultIcon(c).amend(
      fontSize := "4.5em",
      position := "absolute",
      top := "50%",
      left := "50%",
      transform := "translate(-50%,-65%)"
    ),
    div(
      Seq(
        fontSize := "1.25em",
        textAlign := "center",
        position := "absolute",
        left := "1em",
        right := "1em",
        bottom := "25%",
        transform := "translateY(50%)",
        lineHeight := "1em"
      ),
      c match {
        case MissionCard.Fail => "Fail"
        case MissionCard.Success => "Success"
      }
    )
  )

  def sortPlayers(
      maybeUserId: Option[PlayerId],
      players: List[Player]
  ): List[Player] = {
    val userIdx = players.indexWhere(_.id.some === maybeUserId)
    userIdx match {
      case -1 => players
      case idx =>
        val (beforePlayer, rest) = players.splitAt(idx)
        List.concat(rest, beforePlayer)
    }
  }

  implicit class Intersperse[A](private val self: List[A]) {
    def intersperse(a: => A): List[A] = self match {
      case a1 :: a2 :: Nil => a1 :: a :: a2 :: Nil
      case a1 :: a2 :: t => a1 :: a :: a2 :: a :: t.intersperse(a)
      case as => as
    }
  }

  implicit class PlayerOps(private val self: Player) {
    def isThinking(state: State): Boolean =
      state.phase match {
        case Phase.RoleAssigning(ready) => !ready.contains(self.id)
        case Phase.TeamProposing => self.id === state.leader
        case phase: Phase.TeamVoting => !phase.votes.contains(self.id)
        case phase: TeamVoteReveal => !phase.ready.contains(self.id)
        case phase: Phase.MissionInProgress =>
          phase.cards.get(self.id) === Some(None)
        case phase: Phase.MissionReveal =>
          !phase.ready.contains(self.id)
        case Phase.Assassination => self.hand === CharacterCard.Assassin
        case _: Phase.GameComplete => false
      }

    def canClickReady(state: State): Boolean =
      state.phase match {
        case Phase.RoleAssigning(ready) => !ready.contains(self.id)
        case Phase.TeamProposing => false
        case _: Phase.TeamVoting => false
        case phase: Phase.TeamVoteReveal => !phase.ready.contains(self.id)
        case _: MissionInProgress => false
        case phase: Phase.MissionReveal => !phase.ready.contains(self.id)
        case Phase.Assassination => false
        case _: Phase.GameComplete => false

      }
  }
}
