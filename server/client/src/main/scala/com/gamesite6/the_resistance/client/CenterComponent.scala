package com.gamesite6.the_resistance.client

import cats.implicits._
import com.gamesite6.the_resistance.shared._
import com.raquo.airstream.signal.Signal
import com.raquo.laminar.api.L._
import com.raquo.laminar.builders.HtmlBuilders

object CenterComponent extends HtmlBuilders {
  def apply()(implicit
              userSignal: Signal[Option[Player]],
              stateSignal: Signal[State],
              actionBus: WriteBus[Action]) = {

    def roleAssigning(state: State, maybeUser: Option[Player]) = {
      maybeUser
        .filter(_.canClickReady(state)) match {
        case Some(user) =>
          div(
            Seq(
              textAlign := "center"
            ),
            h3(
              "You are ",
              user.hand match {
                case CharacterCard.Merlin => "Merlin"
                case CharacterCard.Operative =>
                  "a resistance operative"
                case CharacterCard.Assassin => "the assassin"
                case CharacterCard.Spy => "a spy"
              },
              "!"
            ),
            p(
              characterCardFront(user.hand)
                .amend(
                  margin := "auto"
                )
            ),
            user.hand match {
              case CharacterCard.Merlin | CharacterCard.Assassin |
                  CharacterCard.Spy =>
                p(
                  strong("Spies: "),
                  state.players
                    .filter(_.hand.affiliation === Affiliation.Spies)
                    .map(player => span(gs6User(player.id)))
                    .intersperse(span(", "))
                )

              case CharacterCard.Operative => emptyNode
            },
            button(
              cls := "primary",
              onClick.map(_ => Action.Ready) --> actionBus,
              "I'm ready!"
            )
          )
        case None =>
          p(
            textAlign := "center",
            em("Players are looking at their character cards.")
          )
      }
    }

    def teamProposing(
        state: State,
        maybeUser: Option[Player]
    ) = {
      val teamSize = Mission.teamSize(
        playerCount = state.players.size,
        missionIndex = state.missions.size
      )

      final case class PlayerCheckbox(playerId: PlayerId, selected: Boolean)

      val checkboxesVar = Var[List[PlayerCheckbox]] {
        state.players.map(player => PlayerCheckbox(player.id, selected = false))
      }

      if (state.leader.some === maybeUser.map(_.id)) {
        div(
          Seq(
            display := "flex",
            flexDirection := "column",
            alignItems := "center"
          ),
          em(s"You must propose a team of $teamSize players."),
          ul(
            cls := "gs6-the-resistance-user-list",
            margin := "0",
            children <-- checkboxesVar.signal.split(_.playerId) {
              case (playerId, _, signal) =>
                li(
                  label(
                    input(
                      `type` := "checkbox",
                      checked <-- signal.map(_.selected),
                      onClick --> Observer[Any] { _ =>
                        checkboxesVar.update { current =>
                          val selected = current
                            .find(_.playerId === playerId)
                            .exists(_.selected)

                          val idx =
                            current.indexWhere(_.playerId === playerId)

                          current.updated(
                            idx,
                            PlayerCheckbox(
                              playerId,
                              selected = !selected
                            )
                          )
                        }
                      }
                    ),
                    " ",
                    gs6User(playerId)
                  )
                )
            }
          ),
          p(
            button(
              cls := "primary",
              disabled <-- checkboxesVar.signal.map(
                _.count(_.selected) =!= teamSize
              ),
              onClick.map(_ => {
                val team = checkboxesVar
                  .now()
                  .filter(_.selected)
                  .map(_.playerId)
                  .toSet

                Action.ProposeTeam(team)
              }) --> actionBus,
              "Propose team!"
            ))
        )
      } else {
        div(
          textAlign := "center",
          p(
            em(
              gs6User(state.leader),
              s" must propose a team of $teamSize players."
            )
          )
        )
      }
    }

    def teamVoting(
        state: State,
        phase: Phase.TeamVoting,
        maybeUser: Option[Player]
    ) = {

      div(
        Seq(
          display := "flex",
          flexDirection := "column",
          alignItems := "center"
        ),
        h3(s"Vote for Mission ${state.missions.size + 1} Team"),
        ul(
          cls := "gs6-the-resistance-user-list",
          phase.team.toList.map(playerId => li(gs6User(playerId)))
        ),
        p("Proposed by ", gs6User(state.leader)),
        maybeUser match {
          case Some(user) if !phase.votes.contains(user.id) =>
            div(
              button(
                cls := "secondary",
                span(cls := "emoji", raw"👎"),
                " Reject",
                onClick.map(_ => Action.VoteTeam(false)) --> actionBus
              ),
              " ",
              button(
                cls := "primary",
                span(cls := "emoji", raw"👍"),
                " Approve",
                onClick.map(_ => Action.VoteTeam(true)) --> actionBus
              )
            )
          case _ => p(em("Players are voting …"))
        }
      )
    }

    def teamVoteReveal(
        state: State,
        phase: Phase.TeamVoteReveal,
        maybeUser: Option[Player]
    ) =
      div(
        Seq(
          display("flex"),
          flexDirection("column")
        ),
        h3(
          textAlign("center"),
          if (phase.voteSuccessful) "Vote succeeded" else "Vote failed"
        ),
        div(
          Seq(
            display("flex"),
            justifyContent("space-around")
          ),
          div(
            h4(
              span(cls := "emoji", raw"👎"),
              " Voted to reject"
            ),
            hr(),
            ul(
              cls := "gs6-the-resistance-user-list",
              phase.votes
                .filter { case (_, vote) => !vote }
                .keys
                .toList
                .map(playerId => li(gs6User(playerId)))
            )
          ),
          div(
            h4(
              span(cls := "emoji", raw"👍"),
              " Voted to approve"
            ),
            hr(),
            ul(
              cls := "gs6-the-resistance-user-list",
              phase.votes
                .filter { case (_, vote) => vote }
                .keys
                .toList
                .map(playerId => li(gs6User(playerId)))
            )
          )
        ),
        maybeUser
          .filter(_.canClickReady(state))
          .as(
            div(
              textAlign("center"),
              button(
                cls := "primary",
                onClick.map(_ => Action.Ready) --> actionBus,
                "I'm ready!"
              )
            )
          )
      )

    def missionInProgress(
        state: State,
        phase: Phase.MissionInProgress,
        maybeUser: Option[Player]
    ) =
      div(
        Seq(
          display := "flex",
          flexDirection := "column",
          alignItems := "center"
        ),
        h3(s"Mission #${state.missions.size + 1} in progress"),
        div(
          ul(
            cls := "gs6-the-resistance-user-list",
            phase.cards.keys.toList.map(playerId => li(gs6User(playerId)))
          )
        ),
        maybeUser match {
          case Some(user) if phase.cards.get(user.id) === Some(None) =>
            div(
              button(
                cls := "gs6-the-resistance-card-button",
                disabled := user.isResistance,
                onClick.map(_ => Action.PlayMissionCard(MissionCard.Fail))
                  --> actionBus,
                missionCardFront(MissionCard.Fail),
              ),
              " ",
              button(
                cls := "gs6-the-resistance-card-button",
                onClick.map(_ => Action.PlayMissionCard(MissionCard.Success))
                  --> actionBus,
                missionCardFront(MissionCard.Success)
              )
            )
          case _ => emptyNode
        }
      )

    def missionReveal(
        state: State,
        phase: Phase.MissionReveal,
        maybeUser: Option[Player]
    ) = {

      val missionSuccessful = phase.missionSuccessful(
        playerCount = state.players.size,
        missionIndex = state.missions.size
      )

      div(
        Seq(
          textAlign := "center"
        ),
        h2(if (missionSuccessful) "Mission successful!" else "Mission failed!"),
        p(
          Seq(
            display := "flex",
            justifyContent := "center",
          ),
          phase.cards.map(
            card =>
              missionCardFront(card).amend(
                fontSize := "0.5em",
                marginLeft := "1em",
                marginRight := "1em"
            ))
        ),
        maybeUser
          .filter(_.canClickReady(state))
          .as(
            p(
              button(
                cls := "primary",
                onClick.map(_ => Action.Ready) --> actionBus,
                "I'm ready!"
              )
            )
          )
      )
    }

    def assassination(state: State, maybeUser: Option[Player]) =
      div(
        Seq(
          display := "flex",
          flexDirection := "column",
          alignItems := "center"
        ),
        h3("Spies have one last chance!"),
        state.players.find(_.hand === CharacterCard.Assassin).map {
          assassinPlayer =>
            if (assassinPlayer.id.some === maybeUser.map(_.id)) {

              val targets = state.players
                .filter(_.isResistance)
                .map(_.id)

              val targetVar = Var(targets.head)

              Seq(
                p(
                  textAlign := "center",
                  em("You must guess which player is Merlin!"),
                  br(),
                  em(
                    fontWeight := 300,
                    "You may discuss freely with the other spies."
                  )
                ),
                form(
                  onSubmit.preventDefault
                    .map(_ => Action.Assassinate(targetVar.now())) --> actionBus,
                  ul(
                    cls := "gs6-the-resistance-user-list",
                    targets.map { targetId =>
                      li(
                        label(
                          input(
                            `type` := "radio",
                            checked <-- targetVar.signal.map(_ === targetId),
                            name := "merlin-target",
                            onClick.map(_ => targetId) --> targetVar.writer,
                          ),
                          " ",
                          gs6User(targetId)
                        )
                      )

                    }
                  ),
                  button(
                    cls := "primary",
                    `type` := "submit",
                    "That's Merlin!"
                  )
                )
              )
            } else
              Seq(
                p(
                  em(
                    gs6User(assassinPlayer.id),
                    " must guess which player is Merlin."
                  ),
                  br(),
                  em(
                    fontWeight := 300,
                    "They may discuss freely with the other spies."
                  )
                ),
              )
        }
      )

    def missionSummary(
        maybeMission: Option[Mission],
        playerCount: PlayerCount,
        missionIndex: Int
    ) =
      div(
        Seq(
          textAlign := "center"
        ),
        s"Mission ${missionIndex + 1}",
        div(
          Seq(
            fontWeight := 500,
            fontSize := "2em"
          ),
          maybeMission match {
            case Some(mission) => teamIcon(mission.winner)
            case None =>
              Mission
                .teamSize(
                  playerCount = playerCount,
                  missionIndex = missionIndex
                )
                .toString
          }
        ),
        Mission
          .failsRequired(playerCount = playerCount, missionIndex = missionIndex)
          .some
          .filter(_ === 2)
          .as(
            small("Two fails required")
          )
      )

    def gameComplete(phase: Phase.GameComplete) = div(
      Seq(
        textAlign := "center"
      ),
      p(
        teamIcon(phase.winners).amend(
          fontSize := "5em",
        )
      ),
      phase.winners match {
        case Affiliation.Resistance => h2("The Resistance wins!")
        case Affiliation.Spies => h2("The spies win!")
      },
    )

    div(
      child <-- stateSignal.combineWith(userSignal).map {
        case (state, maybeUser) =>
          state.phase match {
            case Phase.RoleAssigning(_) => roleAssigning(state, maybeUser)
            case Phase.TeamProposing =>
              teamProposing(state, maybeUser)
            case phase: Phase.TeamVoting =>
              teamVoting(state, phase, maybeUser)
            case phase: Phase.TeamVoteReveal =>
              teamVoteReveal(state, phase, maybeUser)
            case phase: Phase.MissionInProgress =>
              missionInProgress(state, phase, maybeUser)
            case phase: Phase.MissionReveal =>
              missionReveal(state, phase, maybeUser)
            case Phase.Assassination =>
              assassination(state, maybeUser)
            case phase: Phase.GameComplete =>
              gameComplete(phase)
          }
      },
      hr(),
      div(
        textAlign := "center",
        div(
          Seq(
            display := "flex"
          ),
          children <-- stateSignal
            .map(
              _.missions
                .map(_.some)
                .appendedAll(List.fill(5)(None))
                .take(5)
                .zipWithIndex
            )
            .split { case (_, idx: Int) => idx } {
              case (_, _, signal) =>
                div(
                  Seq(
                    marginLeft := "0.5em",
                    marginRight := "0.5em"
                  ),
                  child <-- (for {
                    signalValue <- signal
                    playerCount <- stateSignal.map(_.players.size)
                  } yield
                    signalValue match {
                      case (maybeMission, idx) =>
                        missionSummary(
                          playerCount = playerCount,
                          maybeMission = maybeMission,
                          missionIndex = idx
                        )

                    })
                )
            },
        ),
        div(
          s"Failed votes: ",
          child <-- stateSignal.map { state =>
            state.phase match {
              case phase: Phase.TeamVoteReveal if !phase.voteSuccessful =>
                span(
                  s"${state.failedVotes}/5",
                  strong(" ⟶ "),
                  strong(
                    fontSize := "1.1em",
                    s"${state.failedVotes + 1}/5"
                  )
                )
              case _ => s"${state.failedVotes}/5"
            }

          },
        )
      )
    )
  }
}
