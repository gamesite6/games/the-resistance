ThisBuild / organization := "com.gamesite6"
ThisBuild / scalaVersion := "2.13.5"

lazy val root = (project in file("."))
  .settings(
    name := "the-resistance"
  )
  .aggregate(shared, server, client)

lazy val shared = (project in file("shared"))
  .settings(
    name := "the-resistance-shared",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      "io.circe"     %% "circe-generic"        % "0.14.1",
      "io.circe"     %% "circe-generic-extras" % "0.14.1",
      "io.circe"     %% "circe-literal" % "0.14.1",
      "io.circe"     %% "circe-parser"         % "0.14.1" % "test",
      "com.beachape" %% "enumeratum"           % "1.6.1",
      "com.beachape" %% "enumeratum-circe"     % "1.6.1",
      "com.lihaoyi"  %% "utest"                % "0.7.4"  % "test"
    ),
    testFrameworks += new TestFramework("utest.runner.Framework"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    scalacOptions ++= Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-language:higherKinds",
      "-language:postfixOps",
      "-feature",
      "-Xfatal-warnings"
    )
  )

lazy val client = (project in file("client"))
  .enablePlugins(ScalaJSPlugin)
  .dependsOn(shared)
  .settings(
    name := "the-resistance-client",
    version := "0.1.0-SNAPSHOT",
    scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom"      % "1.0.0",
      "com.raquo"    %%% "laminar"          % "0.9.1",
      "io.circe"     %%% "circe-generic"    % "0.14.1",
      "io.circe"     %%% "circe-scalajs"    % "0.14.1",
      "io.circe"     %%% "circe-parser"     % "0.14.1" % "test",
      "com.beachape" %%% "enumeratum"       % "1.6.1",
      "com.beachape" %%% "enumeratum-circe" % "1.6.1"
    ),
    scalacOptions ++= Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-language:higherKinds",
      "-language:postfixOps",
      "-feature",
      "-Xfatal-warnings"
    )
  )

lazy val server = (project in file("server"))
  .dependsOn(shared)
  .settings(
    name := "the-resistance-server",
    version := "0.1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      "org.http4s"                 %% "http4s-blaze-server" % "0.21.4",
      "org.http4s"                 %% "http4s-blaze-client" % "0.21.4",
      "org.http4s"                 %% "http4s-circe"        % "0.21.4",
      "org.http4s"                 %% "http4s-dsl"          % "0.21.4",
      "io.circe"                   %% "circe-generic"       % "0.14.1",
      "ch.qos.logback"              % "logback-classic"     % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging"       % "3.9.2",
      "com.beachape"               %% "enumeratum"          % "1.6.1",
      "com.beachape"               %% "enumeratum-circe"    % "1.6.1",
      "com.lihaoyi"                %% "utest"               % "0.7.4" % "test"
    ),
    testFrameworks += new TestFramework("utest.runner.Framework"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.10.3"),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    scalacOptions ++= Seq(
      "-deprecation",
      "-encoding",
      "UTF-8",
      "-language:higherKinds",
      "-language:postfixOps",
      "-feature",
      "-Xfatal-warnings"
    )
  )
