package com.gamesite6.the_resistance.server

import cats.implicits._
import com.gamesite6.the_resistance.shared.Phase.TeamVoting
import com.gamesite6.the_resistance.shared._

object PerformAction {

  def actionIsAllowed(
      action: Action,
      performedBy: PlayerId,
      state: State
  ): Boolean = {
    val playerIds: Set[PlayerId] = state.players.map(_.id).toSet

    if (!playerIds.contains(performedBy))
      false
    else
      (state.phase, action) match {
        case (phase: Phase.RoleAssigning, Action.Ready) =>
          !phase.ready.contains(performedBy)
        case (_: Phase.RoleAssigning, _) => false
        case (Phase.TeamProposing, action: Action.ProposeTeam) =>
          state.leader === performedBy &&
            action.team.subsetOf(playerIds) &&
            action.team.size === Mission.teamSize(
              playerCount = playerIds.size,
              missionIndex = state.missions.length
            )
        case (Phase.TeamProposing, _) => false

        case (phase: Phase.TeamVoting, _: Action.VoteTeam) =>
          !phase.votes.contains(performedBy)
        case (_: Phase.TeamVoting, _) => false

        case (phase: Phase.TeamVoteReveal, Action.Ready) =>
          !phase.ready.contains(performedBy)
        case (_: Phase.TeamVoteReveal, _) => false

        case (
            phase: Phase.MissionInProgress,
            Action.PlayMissionCard(missionCard)
            ) =>
          state.players.find(_.id === performedBy).exists { player =>
            val isInTeam = phase.cards.keySet.contains(player.id)
            val hasNotPlayedCard = phase.cards.get(player.id) === Some(None)
            val cardIsValid =
              player.isSpy || missionCard === MissionCard.Success
            isInTeam && hasNotPlayedCard && cardIsValid
          }

        case (_: Phase.MissionInProgress, _) => false

        case (phase: Phase.MissionReveal, Action.Ready) =>
          !phase.ready.contains(performedBy)
        case (_: Phase.MissionReveal, _) => false

        case (Phase.Assassination, Action.Assassinate(target)) =>
          (for {
            player <- state.players.find(_.id === performedBy)
            targetPlayer <- state.players.find(_.id === target)
          } yield
            player.hand === CharacterCard.Assassin && targetPlayer.isResistance)
            .getOrElse(false)

        case (Phase.Assassination, _) => false

        case (_: Phase.GameComplete, _) => false

      }
  }

  def apply(
      action: Action,
      performedBy: PlayerId,
      state: State,
      settings: Settings,
      seed: Seed
  ): Option[State] = {

    val playerIds: Set[PlayerId] = state.players.map(_.id).toSet

    if (!actionIsAllowed(action, performedBy, state))
      None
    else {
      val nextState: Option[State] = (state.phase, action) match {
        case (phase: Phase.RoleAssigning, Action.Ready) =>
          val nextReady = phase.ready + performedBy

          val nextPhase =
            if (nextReady === playerIds)
              Phase.TeamProposing
            else
              Phase.RoleAssigning(ready = nextReady)

          Some(state.copy(phase = nextPhase))

        case (_: Phase.RoleAssigning, _) => None

        case (Phase.TeamProposing, action: Action.ProposeTeam) =>
          val missionIndex = state.missions.size
          val teamSize = Mission.teamSize(
            playerCount = state.players.size,
            missionIndex = missionIndex
          )

          val gamePlayers: Set[PlayerId] = state.players.map(_.id).toSet

          if (action.team.size === teamSize &&
              action.team.subsetOf(gamePlayers))
            state
              .copy(
                phase = TeamVoting(
                  team = action.team,
                  votes = Map.empty
                )
              )
              .some
          else
            None

        case (Phase.TeamProposing, _) => None

        case (phase: Phase.TeamVoting, action: Action.VoteTeam) =>
          val nextVotes = phase.votes + (performedBy -> action.vote)
          if (nextVotes.size === state.players.size) {
            state
              .copy(
                phase = Phase.TeamVoteReveal(
                  team = phase.team,
                  votes = nextVotes,
                  ready = Set.empty
                )
              )
              .some
          } else
            state.copy(phase = phase.copy(votes = nextVotes)).some

        case (_: Phase.TeamVoting, _) => None

        case (phase: Phase.TeamVoteReveal, _) =>
          val nextReady = phase.ready + performedBy
          if (nextReady === playerIds) {
            if (phase.voteSuccessful)
              state
                .copy(
                  failedVotes = 0,
                  phase = Phase.MissionInProgress(
                    cards = phase.team.toList.map((_, None)).toMap
                  )
                )
                .some
            else {
              val failedVotes = state.failedVotes + 1

              if (failedVotes >= 5)
                state
                  .copy(failedVotes = failedVotes)
                  .withPhase(Phase.GameComplete(Affiliation.Spies))
                  .some
              else
                state
                  .copy(failedVotes = failedVotes)
                  .withPhase(Phase.TeamProposing)
                  .withNextLeader
                  .some
            }

          } else
            state.copy(phase = phase.copy(ready = nextReady)).some

        case (phase: Phase.MissionInProgress, action: Action.PlayMissionCard) =>
          val nextMissionCards = phase.cards + (performedBy -> action.card.some)
          if (nextMissionCards.values.forall(_.isDefined))
            state
              .copy(
                phase = Phase.MissionReveal(
                  team = nextMissionCards.keySet,
                  cards = nextMissionCards.values.flatten.toList,
                  ready = Set.empty
                )
              )
              .some
          else
            state.copy(phase = phase.copy(nextMissionCards)).some
        case (_: Phase.MissionInProgress, _) => None

        case (phase: Phase.MissionReveal, Action.Ready) =>
          val nextReady = phase.ready + performedBy
          if (nextReady.size === state.players.size) {
            val missionSuccessful = phase.missionSuccessful(
              playerCount = state.players.size,
              missionIndex = state.missions.size
            )
            val mission = Mission(
              team = phase.team,
              cards = phase.cards,
              winner =
                if (missionSuccessful) Affiliation.Resistance
                else Affiliation.Spies
            )

            val nextState = state.copy(
              missions = state.missions.appended(mission)
            )

            nextState.missions
              .groupBy(_.winner)
              .fmap(_.size)
              .find { case (_, score) => score >= 3 }
              .map { case (team, _) => team } match {
              case Some(Affiliation.Spies) =>
                nextState
                  .withPhase(Phase.GameComplete(Affiliation.Spies))
                  .some

              case Some(Affiliation.Resistance) =>
                nextState
                  .withPhase(Phase.Assassination)
                  .some

              case None =>
                nextState
                  .withPhase(Phase.TeamProposing)
                  .withNextLeader
                  .some
            }

          } else {
            state.withPhase(phase.copy(ready = nextReady)).some
          }

        case (_: Phase.MissionReveal, _) => None

        case (Phase.Assassination, Action.Assassinate(target)) =>
          for {
            player <- state.players.find(_.id === performedBy)
            targetPlayer <- state.players.find(_.id === target)
            speech = state.speech +
              (player.id -> Speech.Assassinated(target)) +
              (target -> Speech.WasAssassinated)
          } yield
            (targetPlayer.hand match {
              case CharacterCard.Merlin =>
                state.withPhase(Phase.GameComplete(Affiliation.Spies))
              case _ =>
                state.withPhase(Phase.GameComplete(Affiliation.Resistance))
            }).copy(speech = speech)

        case (Phase.Assassination, _) => None

        case (_: Phase.GameComplete, _) => None

      }

      nextState
    }
  }
}
