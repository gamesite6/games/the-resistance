package com.gamesite6.the_resistance.server

import com.gamesite6.the_resistance.shared._
import scala.util.Random

object InitialState {
  def apply(
      players: Set[PlayerId],
      settings: Settings,
      seed: Seed
  ): Option[State] = {
    val rng = new Random(seed)

    val shuffledPlayers: List[PlayerId] = rng.shuffle(players.toList.sorted)

    for {
      characters <- CharacterCard.getCharacterCards(settings.gameMode,
                                                    players.size)

      shuffledCharacters = rng.shuffle(characters)

      players = shuffledPlayers.zip(shuffledCharacters).map {
        case (playerId, card) => Player(id = playerId, hand = card)
      }
      leader <- shuffledPlayers.headOption
    } yield
      State(
        players = players,
        leader = leader,
        phase = Phase.RoleAssigning(ready = Set.empty),
        missions = List.empty,
        failedVotes = 0,
        speech = Map.empty
      )

  }
}
