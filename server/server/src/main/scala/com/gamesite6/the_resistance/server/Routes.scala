package com.gamesite6.the_resistance.server

import cats.effect.Sync
import com.gamesite6.the_resistance.shared.dto.{
  InitialStateReq,
  InitialStateRes,
  PerformActionReq,
  PerformActionRes
}
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe._
import cats.implicits._
import com.gamesite6.the_resistance.shared.Phase

class Routes[F[_]: Sync] extends Http4sDsl[F] {

  def gameRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root / "initial-state" =>
      for {
        initialStateReq <- req.as[InitialStateReq]
        res <- InitialState(
          players = initialStateReq.players,
          settings = initialStateReq.settings,
          seed = initialStateReq.seed
        ) match {
          case Some(state) => Ok(InitialStateRes(state))
          case None => UnprocessableEntity("")
        }
      } yield res

    case req @ POST -> Root / "perform-action" =>
      for {
        performActionReq <- req.as[PerformActionReq]
        res <- PerformAction(
          action = performActionReq.action,
          performedBy = performActionReq.performedBy,
          state = performActionReq.state,
          settings = performActionReq.settings,
          seed = performActionReq.seed
        ) match {
          case Some(nextState) =>
            Ok(
              PerformActionRes(
                completed = nextState.phase match {
                  case _: Phase.GameComplete => true
                  case _ => false
                },
                nextState = nextState,
              ))
          case None => UnprocessableEntity("")
        }

      } yield res
  }

  implicit val initialStateReqEntityDecoder: EntityDecoder[F, InitialStateReq] =
    jsonOf

  implicit val initialStateResEntityEncoder: EntityEncoder[F, InitialStateRes] =
    jsonEncoderOf

  implicit val performActionReqEntityDecoder
    : EntityDecoder[F, PerformActionReq] = jsonOf

  implicit val performActionResEntityEncoder
    : EntityEncoder[F, PerformActionRes] = jsonEncoderOf

}
