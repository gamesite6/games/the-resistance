package com.gamesite6.the_resistance.server

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext.global

object Main extends IOApp with LazyLogging {
  def run(args: List[String]): IO[ExitCode] =
    for {

      port <- IO(sys.env("PORT"))
        .onError(_ => IO(logger.error("missing PORT")))
        .map(_.toInt)

      httpApp = new Routes[IO].gameRoutes.orNotFound

      exitCode <- BlazeServerBuilder[IO](global)
        .bindHttp(port, "0.0.0.0")
        .withHttpApp(httpApp)
        .serve
        .compile
        .drain
        .as(ExitCode.Success)

    } yield exitCode
}
