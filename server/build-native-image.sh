#!/bin/bash

native-image --static \
  -H:+ReportExceptionStackTraces \
  -H:+AddAllCharsets \
  --allow-incomplete-classpath \
  --no-fallback \
  --initialize-at-build-time \
  --enable-http \
  --enable-https \
  --enable-all-security-services \
  --verbose \
  -jar "./server/target/scala-2.13/the-resistance-server-assembly-0.1.0-SNAPSHOT.jar" \
  the-resistance-server-binary