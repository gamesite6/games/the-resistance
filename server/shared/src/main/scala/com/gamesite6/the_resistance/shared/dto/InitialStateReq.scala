package com.gamesite6.the_resistance.shared.dto

import cats.Eq
import com.gamesite6.the_resistance.shared._
import io.circe.Decoder
import io.circe.generic.semiauto._

final case class InitialStateReq(
    players: Set[PlayerId],
    settings: Settings,
    seed: Seed
)

object InitialStateReq {
  implicit val decodeInitialStateReq: Decoder[InitialStateReq] = deriveDecoder
  implicit val eqInitialStateReq: Eq[InitialStateReq] = Eq.fromUniversalEquals
}
