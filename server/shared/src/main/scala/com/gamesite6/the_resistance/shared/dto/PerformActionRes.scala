package com.gamesite6.the_resistance.shared.dto

import com.gamesite6.the_resistance.shared.State
import io.circe.Encoder
import io.circe.generic.semiauto.deriveEncoder

final case class PerformActionRes(completed: Boolean, nextState: State)

object PerformActionRes {
  implicit val encodePerformActionRes: Encoder[PerformActionRes] =
    deriveEncoder
}
