package com.gamesite6.the_resistance.shared.dto

import com.gamesite6.the_resistance.shared.State
import io.circe.Encoder
import io.circe.generic.semiauto._

final case class InitialStateRes(state: State)

object InitialStateRes {
  implicit val encodeInitialStateRes: Encoder[InitialStateRes] = deriveEncoder
}
