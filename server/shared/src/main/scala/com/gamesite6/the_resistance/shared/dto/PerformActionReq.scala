package com.gamesite6.the_resistance.shared.dto

import com.gamesite6.the_resistance.shared._
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

case class PerformActionReq(
    performedBy: PlayerId,
    action: Action,
    state: State,
    settings: Settings,
    seed: Seed
)

object PerformActionReq {
  implicit val decodePerformActionReq: Decoder[PerformActionReq] =
    deriveDecoder
}
