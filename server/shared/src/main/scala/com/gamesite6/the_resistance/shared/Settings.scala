package com.gamesite6.the_resistance.shared

import cats.Eq
import io.circe.Codec
import io.circe.generic.semiauto._

final case class Settings(
    gameMode: GameMode,
    selectedPlayerCounts: Set[Int]
)

object Settings {
  implicit val codecSettings: Codec[Settings] = deriveCodec
  implicit val eqSettings: Eq[Settings] = Eq.fromUniversalEquals

}

sealed trait GameMode extends Product with Serializable
object GameMode {
  case object Avalon extends GameMode

  implicit val codecGameMode: Codec[GameMode] = deriveCodec
  implicit val eqGameMode: Eq[GameMode] = Eq.fromUniversalEquals
}
