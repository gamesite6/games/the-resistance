package com.gamesite6.the_resistance.shared

import io.circe.Codec
import io.circe.generic.semiauto._

sealed trait Action extends Product with Serializable

object Action {
  case object Ready extends Action
  case class ProposeTeam(team: Set[PlayerId]) extends Action
  case class VoteTeam(vote: Boolean) extends Action
  case class PlayMissionCard(card: MissionCard) extends Action
  case class Assassinate(target: PlayerId) extends Action

  implicit val codecAction: Codec[Action] = deriveCodec
}
