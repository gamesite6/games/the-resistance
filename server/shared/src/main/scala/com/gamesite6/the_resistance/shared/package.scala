package com.gamesite6.the_resistance

package object shared {
  type PlayerId = Int

  type PlayerCount = Int

  type Seed = Long
}
