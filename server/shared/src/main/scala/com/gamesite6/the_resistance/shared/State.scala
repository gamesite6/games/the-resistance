package com.gamesite6.the_resistance.shared

import cats.implicits._
import cats.kernel.Eq
import enumeratum._
import io.circe.Codec
import io.circe.generic.semiauto._

final case class State(
    players: List[Player],
    leader: PlayerId,
    phase: Phase,
    missions: List[Mission],
    failedVotes: Int,
    speech: Map[PlayerId, Speech]
)

object State {
  implicit val stateCodec: Codec[State] = deriveCodec

  implicit class StateOps(private val self: State) {
    def withNextLeader: State = {
      val leaderIdx = self.players.indexWhere(_.id === self.leader)
      val nextLeaderIdx = (leaderIdx + 1) % self.players.size
      val nextLeader = self.players(nextLeaderIdx)

      self.copy(leader = nextLeader.id)
    }

    def withPhase(phase: Phase): State =
      self.copy(phase = phase)

    def clearSpeech: State =
      self.copy(speech = Map.empty)

    def winningTeam: Option[Affiliation] =
      if (self.failedVotes >= 5)
        Affiliation.Spies.some
      else
        self.missions
          .groupBy(_.winner)
          .fmap(_.size)
          .find { case (_, score) => score >= 3 }
          .map { case (team, _) => team }
  }

}

final case class Player(id: PlayerId, hand: CharacterCard)

object Player {
  implicit val codecPlayer: Codec[Player] = deriveCodec

  implicit class PlayerOps(private val self: Player) {
    def isSpy: Boolean = self.hand.affiliation === Affiliation.Spies
    def isResistance: Boolean = !isSpy
  }
}

sealed trait Speech extends Product with Serializable
object Speech {
  case class TeamProposed(players: Set[PlayerId]) extends Speech
  case class Assassinated(target: PlayerId) extends Speech
  case object WasAssassinated extends Speech

  implicit val codecSpeech: Codec[Speech] = deriveCodec
}

sealed trait Phase extends Product with Serializable

object Phase {
  case class RoleAssigning(ready: Set[PlayerId]) extends Phase
  case object TeamProposing extends Phase
  case class TeamVoting(
      team: Set[PlayerId],
      votes: Map[PlayerId, Boolean]
  ) extends Phase
  case class TeamVoteReveal(
      team: Set[PlayerId],
      votes: Map[PlayerId, Boolean],
      ready: Set[PlayerId]
  ) extends Phase
  case class MissionInProgress(cards: Map[PlayerId, Option[MissionCard]])
      extends Phase
  case class MissionReveal(
      team: Set[PlayerId],
      cards: List[MissionCard],
      ready: Set[PlayerId]
  ) extends Phase
  case object Assassination extends Phase
  case class GameComplete(winners: Affiliation) extends Phase

  implicit val eqPhase: Eq[Phase] = Eq.fromUniversalEquals
  implicit val codecPhase: Codec[Phase] = deriveCodec

  implicit class TeamVoteRevealOps(private val self: TeamVoteReveal) {
    def voteSuccessful: Boolean = {
      val votesFor = self.votes.values.count(identity)
      val votesAgainst = self.votes.values.count(!_)
      votesFor > votesAgainst
    }
  }

  implicit class MissionRevealOps(private val self: MissionReveal) {
    def missionSuccessful(
        playerCount: PlayerCount,
        missionIndex: Int
    ): Boolean = {
      val failsRequired = Mission.failsRequired(
        playerCount = playerCount,
        missionIndex = missionIndex
      )
      val fails = self.cards.count(_ === MissionCard.Fail)

      fails < failsRequired
    }
  }
}

sealed trait Affiliation extends EnumEntry
object Affiliation extends Enum[Affiliation] with CirceEnum[Affiliation] {
  val values: IndexedSeq[Affiliation] = findValues

  case object Resistance extends Affiliation
  case object Spies extends Affiliation

  implicit val eqAffiliation: Eq[Affiliation] = Eq.fromUniversalEquals
}

sealed trait CharacterCard extends EnumEntry
object CharacterCard extends Enum[CharacterCard] with CirceEnum[CharacterCard] {
  val values: IndexedSeq[CharacterCard] = findValues

  case object Operative extends CharacterCard
  case object Spy extends CharacterCard
  case object Merlin extends CharacterCard
  case object Assassin extends CharacterCard

  implicit val eqCharacterCard: Eq[CharacterCard] = Eq.fromUniversalEquals
  implicit val codecCharacterCard: Codec[CharacterCard] = deriveCodec

  implicit class CharacterOps(private val character: CharacterCard) {
    def affiliation: Affiliation =
      character match {
        case Operative | Merlin => Affiliation.Resistance
        case Spy | Assassin => Affiliation.Spies
      }
  }

  def getCharacterCards(
      gameMode: GameMode,
      playerCount: Int
  ): Option[List[CharacterCard]] =
    for {
      spyCount <- playerCount match {
        case 5 | 6 => Some(2)
        case 7 | 8 | 9 => Some(3)
        case 10 => Some(4)
        case _ => None
      }
      resistanceCount = playerCount - spyCount

    } yield
      List(
        List.fill(spyCount - 1)(Spy),
        List(Assassin),
        List.fill(resistanceCount - 1)(Operative),
        List(Merlin)
      ).flatten

}

sealed trait MissionCard extends EnumEntry
object MissionCard extends Enum[MissionCard] with CirceEnum[MissionCard] {
  override def values: IndexedSeq[MissionCard] = findValues

  case object Fail extends MissionCard
  case object Success extends MissionCard

  implicit val eqMissionCard: Eq[MissionCard] = Eq.fromUniversalEquals
}

final case class Mission(
    team: Set[PlayerId],
    cards: List[MissionCard],
    winner: Affiliation
)

object Mission {
  implicit val codecMission: Codec[Mission] = deriveCodec

  def teamSize(playerCount: PlayerCount, missionIndex: Int): Int = {
    val teamSizes: Map[PlayerCount, List[Int]] = Map(
      5 -> List(2, 3, 2, 3, 3),
      6 -> List(2, 3, 4, 3, 4),
      7 -> List(2, 3, 3, 4, 4),
      8 -> List(3, 4, 4, 5, 5),
      9 -> List(3, 4, 4, 5, 5),
      10 -> List(3, 4, 4, 5, 5)
    )

    (for {
      missions <- teamSizes.get(playerCount)
      teamSize <- missions.lift(missionIndex)
    } yield teamSize).getOrElse(0)

  }

  def failsRequired(playerCount: PlayerCount, missionIndex: Int): Int = {
    if (playerCount > 6 && missionIndex === 3)
      2
    else
      1
  }
}
