package com.gamesite6.the_resistance.shared.dto

import cats.implicits._
import com.gamesite6.the_resistance.shared.{GameMode, Settings}
import io.circe.Json
import io.circe.parser._
import utest._

object InitialStateReqTest extends TestSuite {
  val tests: Tests = Tests {
    "decode json" - {

      val json =
        parse("""{
        "seed": 12345,
        "players": [444, 333, 666, 777, 555, 111, 222],
        "settings": {
          "gameMode": { "Avalon": {} },
          "selectedPlayerCounts": [7, 8, 9, 10]
        }
      }""")

      assertMatch(json) {
        case Right(json: Json) =>
          assert {
            json.as[InitialStateReq] === Right(
              InitialStateReq(
                seed = 12345L,
                players = Set(111, 222, 333, 444, 555, 666, 777),
                settings = Settings(
                  gameMode = GameMode.Avalon,
                  selectedPlayerCounts = Set(7, 8, 9, 10)
                )
              )
            )
          }
      }
    }
  }
}
