package com.gamesite6.the_resistance.shared

import utest._
import io.circe.syntax._
import io.circe.literal._

object JsonTest extends TestSuite {
  override def tests: Tests = Tests {
    "action" - {
      val action: Action = Action.PlayMissionCard(card = MissionCard.Success)
      val json = action.asJson

      assert(json == json"""{ "PlayMissionCard": { "card" : "Success" } }""")
    }
  }
}
