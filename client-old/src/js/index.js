import { GameComponent } from "@gamesite6/the-resistance-game-component";
import "./the-resistance.css";
export class Game extends HTMLElement {
  constructor() {
    super();
  }

  set state(state) {
    this._state = state;
    this.setAttribute("state", JSON.stringify(state));
  }

  set settings(settings) {
    this._settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  get userId() {
    let userId = parseInt(this.getAttribute("user-id"));
    return Number.isInteger(userId) ? userId : undefined;
  }

  connectedCallback() {
    let {
      rootNode,
      stateEventBus,
      settingsEventBus,
      userIdEventBus,
    } = GameComponent.mount(this, {
      userId: this.userId,
      settings: this._settings,
      state: this._state,
      onAction: (action) => {
        this.dispatchEvent(
          new CustomEvent("action", {
            detail: action,
            bubbles: false,
          })
        );
      },
    });

    this.rootNode = rootNode;
    this.stateEventBus = stateEventBus;
    this.settingsEventBus = settingsEventBus;
    this.userIdEventBus = userIdEventBus;
  }

  disconnectedCallback() {
    GameComponent.unmount(this.app);
  }

  static get observedAttributes() {
    return ["user-id", "settings", "state"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case "user-id":
        this.userIdEventBus &&
          GameComponent.updateUserId(this.userIdEventBus, this.userId);
        return;

      case "settings":
        this.settingsEventBus &&
          GameComponent.updateSettings(this.settingsEventBus, this._settings);
        return;

      case "state":
        this.stateEventBus &&
          GameComponent.updateState(this.stateEventBus, this._state);
        return;

      default:
        return;
    }
  }
}

export class Reference extends HTMLElement {
  constructor() {
    super();
  }

  set settings(settings) {
    this._settings = settings;
    this.setAttribute("settings", JSON.stringify(settings));
  }

  connectedCallback() {
    this.innerHTML = `
      <div
        class="panel border shadow"
        style="height: 100%; box-sizing: border-box;"
      >
        <h2>Reference</h2>
        <hr />
        <a
          href="about:blank"
          target="popup"
          onclick="window.open('about:blank','popup','width=600,height=600'); return false;"
        >Rules</a>
      </div>`;
  }

  static get observedAttributes() {
    return ["settings"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (this.app && oldValue !== newValue) {
      this.app.$set({
        settings: this._settings,
      });
    }
  }
}
