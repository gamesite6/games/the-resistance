const path = require("path");

module.exports = {
  entry: path.resolve(__dirname, "src", "js", "index.js"),
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
    libraryTarget: "commonjs2",
  },
  mode: process.env.NODE_ENV,
  resolve: {
    alias: {
      "@gamesite6/the-resistance-game-component":
        process.env.NODE_ENV === "production"
          ? "../../lib/the-resistance-client-opt"
          : "../../lib/the-resistance-client-fastopt",
    },
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
